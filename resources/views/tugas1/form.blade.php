<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
    <h1>Buat Account baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/kirim" method="POST">
        @csrf
        <label >First name:</label> <br> <br>
        <input name="first" type="text"> <br><br>
        <label for="">Last name:</label>
        <br><br>
        <input name="last" type="text" >
        <br><br>
        <label for="">Gender:</label>
        <br><br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender"> Other <br><br>

        <label >Nationality</label>
        <select name="Nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapura">Singapura</option>
        </select>
        <br><br>
        <label >Language Spoken:</label> <br><br>
        <input name="bahasa" type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa" > English <br>
        <input name="bahasa" type="checkbox" > Other <br><br>
        <label >Bio:</label> <br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea>
        <br>
        <input type="submit">


    </form>
</body>
</html>