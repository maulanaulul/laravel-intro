<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    //
    public function index(){
        return view('tugas1.form');
    }

    public function kirim(Request $request){
        $first = $request['first'];
        $last = $request['last'];

        return view('tugas1.welcome', compact('first','last'));
    }
}
